<?php require APPROOT . '/views/inc/header.php';?>

<h1 class="text-center">Dernière étape</h1>

<div class="row">
    <div class="col-md-6 my-md-4 mx-auto">
        <!-- Alert -->
        <div class="row">
            <div class="col-md-12 mx-auto">
                <?=flash('user_message')?>
            </div>
        </div>

        <form action="<?=URLROOT?>/users/finalisation" method="post" enctype="multipart/form-data">

            <!-- Civilite -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="civilite">Civilité</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <select class="custom-select <?=(!empty($data['civilite_err'])) ? 'is-invalid' : ''?>" name="civilite">
                    <option <?=($data['civilite'] == '') ? 'selected' : ''?> disabled>Choisissez votre civilité...</option>
                    <option value="1" <?=($data['civilite'] == 1) ? 'selected' : ''?>>Madame</option>
                    <option value="2" <?=($data['civilite'] == 2) ? 'selected' : ''?>>Monsieur</option>
                </select>
                <div class="invalid-feedback"><?=$data['civilite_err']?></div>
            </div>

            <!-- Nom -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="nom">Nom</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['nom_err'])) ? 'is-invalid' : ''?>" type="text" name="nom" placeholder="ex : Dupont" value="<?=$data['nom']?>">
                <div class="invalid-feedback"><?=$data['nom_err']?></div>
            </div>

            <!-- Prenom -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="prenom">Prénom</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['prenom_err'])) ? 'is-invalid' : ''?>" type="text" name="prenom" placeholder="ex : Pierre" value="<?=$data['prenom']?>">
                <div class="invalid-feedback"><?=$data['prenom_err']?></div>
            </div>

            <!-- Adresse -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="adresse">Adresse</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['adresse_err'])) ? 'is-invalid' : ''?>" type="text" name="adresse" value="<?=$data['adresse']?>" placeholder="ex : 17 rue des Lys">
                <div class="invalid-feedback"><?=$data['adresse_err']?></div>
            </div>

            <!-- Complement -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="complement">Complément</label>
                </div>
                <input class="form-control" type="text" name="complement" aria-describedby="complementHelp" placeholder="ex : Appart. 215, Étage 3, etc." value="<?=$data['complement']?>">
                <small class="form-text text-muted" id="complementHelp">Toutes informations utiles à la livraison.</small>
            </div>
            
            <div class="form-group">
                <div class="form-row d-flex align-items-end">

                    <!-- Ville -->
                    <div class="col mr-3">
                        <div class="row">
                            <label class="col-auto mr-auto" for="ville">Ville</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['ville_err'])) ? 'is-invalid' : ''?>" type="text" name="ville" placeholder="ex : La Rochelle" value="<?=$data['ville']?>">
                        <div class="invalid-feedback"><?=$data['ville_err']?></div>
                    </div>

                    <!-- Code postal -->
                    <div class="col">
                        <div class="row">
                            <label class="col-auto mr-auto" for="cp">Code Postal</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['cp_err'])) ? 'is-invalid' : ''?>" type="text" name="cp" placeholder="ex : 17000" value="<?=$data['cp']?>">
                        <div class="invalid-feedback"><?=$data['cp_err']?></div>
                    </div>
                </div>
            </div>

            <!-- Avatar -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="avartar">Avatar</label>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input <?=(!empty($data['avatar_err'])) ? 'is-invalid' : ''?>" name="avatar" aria-describedby="avatarHelp">
                    <label class="custom-file-label" for="avatar" data-browse="Parcourir...">Choisissez un avatar</label>
                    <div class="invalid-feedback"><?=$data['avatar_err']?></div>
                </div>
                <small class="form-text text-muted" id="avatarHelp">Fichiers acceptés : .png, .jpg, .jpeg</small>
            </div>
            
            <!-- Submit -->
            <button class="btn btn-primary btn-block" type="submit">Finaliser mon compte</button>
        </form>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>