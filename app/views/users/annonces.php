<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1">
    <div class="col-md-10 mx-auto">

        <h1 class="text-center mb-4">Mes annonces</h1>

        <div class="row mb-5">
            <a class="btn btn-primary mx-auto" href="<?=URLROOT?>/mugs/add">Déposer une annonce</a>
        </div>

        <!-- Annonces en attente -->
        <h2 class="text-warning mb-4">Annonces en attente <span class="text-muted">(<?=count($data['mugs_attente'])?>)</span></h2>

        <?php if(!empty($data['mugs_attente'])): ?>
            <table class="table table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Titre</th>
                        <th scope="col">Ajoutée le</th>
                        <th scope="col">Modifiée le</th>
                        <th scope="col">Prix (HT)</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['mugs_attente'] as $mug): ?>
                        <tr>
                            <th class="align-middle" scope="row">
                                <div class="bg-dark rounded bg-img" style="width: 2em; height: 2em; background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                            </th>
                            <td class="align-middle">
                                <a href="<?=URLROOT?>/mugs/<?=$mug->id?>"><?=$mug->titre?></a>
                            </td>
                            <td class="align-middle">
                                <?=$mug->createdAt?>
                            </td>
                            <td class="align-middle">
                                <?=$mug->updatedAt ? $mug->updatedAt : '-'?>
                            </td>
                            <td class="align-middle">
                                <?=$mug->prixHt?>€
                            </td>
                            <td class="align-middle">
                                <form class="text-right" action="<?=URLROOT?>/mugs/delete/<?=$mug->id?>" method="post">
                                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#deleteModal">Supprimer</button>
                                    <!-- Modal -->
                                    <div class="modal fade text-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Avertissement</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Êtes-vous sûr de vouloir supprimer cette annonce ?</p>
                                                <p><strong>Cette action est irréversible.</strong></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                                                <button class="btn btn-danger" type="submit" value="Delete">Supprimer mon annonce</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p class="mt-4 text-center font-italic text-muted">Aucun mug en cours de modération.</p>
        <?php endif ?>


        <hr class="my-5">


        <!-- Annonces refusées -->
        <h2 class="text-danger mb-4">Annonces refusées <span class="text-muted">(<?=count($data['mugs_refuses'])?>)</span></h2>
        
        <?php if(!empty($data['mugs_refuses'])): ?>
            <table class="table table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Titre</th>
                        <th scope="col">Ajoutée le</th>
                        <th scope="col">Modifiée le</th>
                        <th scope="col">Prix (HT)</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['mugs_refuses'] as $mug): ?>
                        <tr>
                            <th class="align-middle" scope="row">
                                <div class="bg-dark rounded bg-img" style="width: 2em; height: 2em; background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                            </th>
                            <td class="align-middle">
                                <a href="<?=URLROOT?>/mugs/<?=$mug->id?>"><?=$mug->titre?></a>
                            </td>
                            <td class="align-middle">
                                <?=$mug->createdAt?>
                            </td>
                            <td class="align-middle">
                                <?=$mug->updatedAt ? $mug->updatedAt : '-'?>
                            </td>
                            <td class="align-middle">
                                <?=$mug->prixHt?>€
                            </td>
                            <td class="align-middle text-right">
                                <a href="<?=URLROOT?>/mugs/edit/<?=$mug->id?>"><button class="btn btn-secondary">Modifier</button></a>
                            </td>
                            <td class="align-middle">
                                <form class="text-right" action="<?=URLROOT?>/mugs/delete/<?=$mug->id?>" method="post">
                                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#deleteModal2">Supprimer</button>
                                    <!-- Modal -->
                                    <div class="modal fade text-left" id="deleteModal2" tabindex="-1" role="dialog" aria-labelledby="deleteModal2Label" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModal2Label">Avertissement</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Êtes-vous sûr de vouloir supprimer cette annonce ?</p>
                                                <p><strong>Cette action est irréversible.</strong></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                                                <button class="btn btn-danger" type="submit" value="Delete">Supprimer mon annonce</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p class="mt-4 text-center font-italic text-muted">Aucun mug refusé.</p>
        <?php endif ?>


        <hr class="my-5">


        <!-- Annonces en vente -->
        <h2 class="text-success mb-4">Annonces en vente <span class="text-muted">(<?=count($data['mugs_publics'])?>)</span></h2>
        
        <?php if(!empty($data['mugs_publics'])): ?>
            <table class="table table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Titre</th>
                        <th scope="col">Ajoutée le</th>
                        <th scope="col">Modifiée le</th>
                        <th scope="col">Prix (HT)</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['mugs_publics'] as $mug): ?>
                    <tr>
                        <th class="align-middle" scope="row">
                            <div class="bg-dark rounded bg-img" style="width: 2em; height: 2em; background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                        </th>
                        <td class="align-middle">
                            <a href="<?=URLROOT?>/mugs/<?=$mug->id?>"><?=$mug->titre?></a>
                        </td>
                        <td class="align-middle">
                            <?=$mug->createdAt?>
                        </td>
                        <td class="align-middle">
                            <?=$mug->updatedAt ? $mug->updatedAt : '-'?>
                        </td>
                        <td class="align-middle">
                            <?=$mug->prixHt?>€
                        </td>
                        <td class="align-middle text-right">
                            <a href="<?=URLROOT?>/mugs/edit/<?=$mug->id?>"><button class="btn btn-secondary">Modifier</button></a>
                        </td>
                        <td class="align-middle">
                            <form class="text-right" action="<?=URLROOT?>/mugs/delete/<?=$mug->id?>" method="post">
                                <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#deleteModal3">Supprimer</button>
                                <!-- Modal -->
                                <div class="modal fade text-left" id="deleteModal3" tabindex="-1" role="dialog" aria-labelledby="deleteModal3Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModal3Label">Avertissement</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Êtes-vous sûr de vouloir supprimer cette annonce ?</p>
                                            <p><strong>Cette action est irréversible.</strong></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                                            <button class="btn btn-danger" type="submit" value="Delete">Supprimer mon annonce</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p class="mt-4 text-center font-italic text-muted">Aucun mug en vente.</p>
        <?php endif ?>


        <hr class="my-5">


        <!-- Annonces vendus -->
        <h2 class="text-secondary mb-4">Mes ventes <span class="text-muted">(<?=count($data['mugs_vendus'])?>)</span></h2>
        
        <?php if(!empty($data['mugs_vendus'])): ?>
            <table class="table table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Titre</th>
                        <th scope="col">Ajoutée le</th>
                        <!-- <th scope="col">Vendue le</th> -->
                        <th scope="col">Prix (HT)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['mugs_vendus'] as $mug): ?>
                        <tr>
                            <th class="align-middle" scope="row">
                                <div class="bg-dark rounded bg-img" style="width: 2em; height: 2em; background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                            </th>
                            <!-- <td class="align-middle">
                                <a href="<?=URLROOT?>/mugs/<?=$mug->id?>"><?=$mug->titre?></a>
                            </td> -->
                            <td class="align-middle">
                                <?=$mug->titre?>
                            </td>
                            <td class="align-middle">
                                <?=$mug->createdAt?>
                            </td>
                            <!-- <td class="align-middle">
                                <?=$mug->updatedAt ? $mug->updatedAt : '-'?>
                            </td> -->
                            <td class="align-middle">
                                <?=$mug->prixHt?>€
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p class="mt-4 text-center font-italic text-muted">Aucun mug vendu.</p>
        <?php endif ?>

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>