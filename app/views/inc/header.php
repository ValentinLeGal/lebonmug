<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=URLROOT?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=URLROOT?>/css/style.css">
    <title><?=SITENAME?></title>
</head>
<body>
    <header><?php require APPROOT . '/views/inc/navbar.php'; ?></header>

    <div class="container py-md-4">