<nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand mr-auto" href="<?=URLROOT?>">LeBonMug</a>
    
    <?php if(isset($_SESSION['user_id'])): ?>
        <?php if($_SESSION['user_role'] == 1 || $_SESSION['user_role'] == 2):?>
            <a class="btn mr-3" href="<?=URLROOT?>/moderators/dashboard">Modération <span class="badge badge-warning">2</span></a>
        <?php endif;?>
        
        <a class="btn mr-2" href="<?=URLROOT?>/mugs/add">Déposer une annonce</a>

        <a class="btn mr-3" href="<?=URLROOT?>/users/panier">Panier <span class="badge badge-secondary d-none">1</span></a>
            
        <!-- <a class="btn mr-3" href="<?=URLROOT?>/users/messages">Messages <span class="badge badge-danger">3</span></a> -->

        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mon compte</button>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?=URLROOT?>/users/annonces">Mes annonces</a>
                <!-- <a class="dropdown-item" href="<?=URLROOT?>/users/achats">Mes achats</a> -->
                <?php if($_SESSION['user_role'] == 1 || $_SESSION['user_role'] == 2):?>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=URLROOT?>/admin/dashboard">Tableau de bord</a>
                <?php endif;?>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=URLROOT?>/users/compte">Paramètres</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-danger" href="<?=URLROOT?>/users/deconnexion">Se déconnecter</a>
            </div>
        </div>
    <?php else: ?>
        <a class="btn mr-3" href="<?=URLROOT?>/users/connexion">Connexion</a>

        <a class="btn btn-primary" href="<?=URLROOT?>/users/inscription">Inscription</a>
    <?php endif; ?>
</nav>