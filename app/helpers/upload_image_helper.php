<?php
	// Upload one image to a folder
	// Ex : upload_image_helper($_FILES['avatar'], '/avatars');
	function upload_image_helper($file, $dest) {
		$fileName = $file['name'];
		$fileTmpName = $file['tmp_name'];
		$fileSize = $file['size'];
		$fileError = $file['error'];
		$fileType = $file['type'];

		$fileExt = explode('.', $fileName);
		$fileActualExt = strtolower(end($fileExt));

		$allowed = array('png', 'jpg', 'jpeg');

		if (in_array($fileActualExt, $allowed)) {
			if ($fileError === 0) {
				$fileNameNew = uniqid('', true) . '.' . $fileActualExt;
				$fileDestination = dirname(APPROOT) . '/public/img' . $dest . '/' . $fileNameNew;
				
				if (move_uploaded_file($fileTmpName, $fileDestination)) {
					return array(true, $fileNameNew);
				} else {
					return array(false, 'Erreur lors de l\'enregistrement de l\'image.');
				}
			} else {
				return array(false, 'Erreur lors de l\'importation de l\'image.');
			}
		} else {
			return array(false, 'Veuillez importer une image (png, jpg ou jpeg).');
		}
	}
?>