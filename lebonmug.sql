-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 09 Mars 2020 à 18:52
-- Version du serveur :  5.7.29-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lebonmug`
--

-- --------------------------------------------------------

--
-- Structure de la table `archiveMugs`
--

CREATE TABLE `archiveMugs` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prix` decimal(6,2) NOT NULL,
  `idVendeur` int(11) NOT NULL,
  `idEtatArticle` int(11) NOT NULL,
  `idStatutAnnonce` int(11) NOT NULL,
  `idCouleur` int(11) NOT NULL,
  `idVolume` int(11) NOT NULL,
  `idType` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT NULL,
  `archivedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `civilitesUser`
--

CREATE TABLE `civilitesUser` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `civilitesUser`
--

INSERT INTO `civilitesUser` (`id`, `libelle`) VALUES
(1, 'Madame'),
(2, 'Monsieur');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `idLivraison` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`id`, `idClient`, `idLivraison`, `createdAt`) VALUES
(8, 5, 1, '2020-02-12 19:37:10');

-- --------------------------------------------------------

--
-- Structure de la table `couleursMug`
--

CREATE TABLE `couleursMug` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `couleursMug`
--

INSERT INTO `couleursMug` (`id`, `libelle`) VALUES
(1, 'Rouge'),
(2, 'Rose'),
(3, 'Jaune'),
(4, 'Vert'),
(5, 'Bleu'),
(6, 'Violet'),
(7, 'Marron'),
(8, 'Noir'),
(9, 'Blanc'),
(10, 'Gris'),
(11, 'Orange');

-- --------------------------------------------------------

--
-- Structure de la table `etatsMug`
--

CREATE TABLE `etatsMug` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etatsMug`
--

INSERT INTO `etatsMug` (`id`, `libelle`) VALUES
(1, 'État neuf'),
(2, 'Très bon état'),
(3, 'Bon état'),
(4, 'Mauvais état'),
(5, 'Très mauvais état');

-- --------------------------------------------------------

--
-- Structure de la table `lignesCommande`
--

CREATE TABLE `lignesCommande` (
  `id` int(11) NOT NULL,
  `idCommande` int(11) NOT NULL,
  `idArticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `lignesPanierUser`
--

CREATE TABLE `lignesPanierUser` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idArticle` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `idAuteur` int(11) NOT NULL,
  `idDestinataire` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `idSujet` int(11) NOT NULL,
  `idStatutMessage` int(11) NOT NULL,
  `idArticle` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mugs`
--

CREATE TABLE `mugs` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prixHt` decimal(6,2) NOT NULL,
  `idVendeur` int(11) NOT NULL,
  `idEtat` int(11) NOT NULL,
  `idStatut` int(11) NOT NULL DEFAULT '1',
  `idCouleur` int(11) NOT NULL,
  `idVolume` int(11) NOT NULL,
  `idType` int(11) NOT NULL,
  `photo1` varchar(255) NOT NULL,
  `photo2` varchar(255) DEFAULT NULL,
  `photo3` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `mugs`
--

INSERT INTO `mugs` (`id`, `titre`, `description`, `prixHt`, `idVendeur`, `idEtat`, `idStatut`, `idCouleur`, `idVolume`, `idType`, `photo1`, `photo2`, `photo3`, `createdAt`, `updatedAt`) VALUES
(4, 'Mug Minion', 'Neuf dans emballage', '5.00', 2, 1, 1, 3, 3, 2, 'mug4_photo1.jpg', 'mug4_photo2.jpg', 'mug4_photo3.jpg', '2020-02-10 20:24:54', '2020-02-11 08:46:00'),
(8, 'Mug Chiens', 'Mug avec 3 têtes de chiens', '2.00', 2, 2, 1, 9, 2, 2, 'mug8_photo1.jpg', 'mug8_photo2.jpg', '', '2020-02-10 22:22:14', '2020-03-09 18:20:34'),
(9, 'Mug Marvel', 'Sur rennes parfait état', '5.00', 5, 2, 1, 5, 3, 5, 'mug9_photo1.jpg', 'mug9_photo2.jpg', NULL, '2020-02-12 21:37:41', NULL),
(10, 'Mug Chat Twinzy', 'Mug Chat Twinzy, hauteur : 10cm,  diamètre : 8cm', '3.00', 5, 1, 1, 9, 2, 2, 'mug10_photo1.jpg', 'mug10_photo2.jpg', 'mug10_photo3.jpg', '2020-02-12 21:43:25', '2020-02-12 22:09:05'),
(11, 'Mug Bart Simpson', 'Mug Bart Simpson en céramique', '3.00', 5, 2, 1, 4, 2, 8, 'mug11_photo1.jpg', 'mug11_photo2.jpg', NULL, '2020-02-12 21:48:16', '2020-02-12 21:51:34'),
(12, 'Mug Rolling Stones', 'Mug des Stones', '10.00', 5, 1, 1, 5, 4, 7, 'mug12_photo1.jpg', 'mug12_photo2.jpg', NULL, '2020-02-12 21:53:43', NULL),
(13, 'Mug Génie d&#39;Aladdin', 'Mug Génie d&#39;Aladdin (Disney). Neuf et emballé', '6.00', 2, 1, 5, 5, 2, 8, 'mug13_photo1.jpg', 'mug13_photo2.jpg', NULL, '2020-02-12 21:58:35', NULL),
(14, 'Mug Mickey & Minnie (Disneyland Paris)', 'Acheté à eurodisney, Disneyland Paris, très bon état', '2.00', 2, 2, 5, 9, 2, 8, 'mug14_photo1.jpg', 'mug14_photo2.jpg', 'mug14_photo3.jpg', '2020-02-12 22:03:59', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rolesUser`
--

CREATE TABLE `rolesUser` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `rolesUser`
--

INSERT INTO `rolesUser` (`id`, `libelle`) VALUES
(1, 'admin'),
(2, 'moderator'),
(3, 'user');

-- --------------------------------------------------------

--
-- Structure de la table `statutsMessage`
--

CREATE TABLE `statutsMessage` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `statutsMug`
--

CREATE TABLE `statutsMug` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `statutsMug`
--

INSERT INTO `statutsMug` (`id`, `libelle`) VALUES
(1, 'Public'),
(2, 'En cours de modération'),
(3, 'Refusé'),
(4, 'Vendu'),
(5, 'Dans panier');

-- --------------------------------------------------------

--
-- Structure de la table `sujetsMessage`
--

CREATE TABLE `sujetsMessage` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `typesLivraison`
--

CREATE TABLE `typesLivraison` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `coutTtc` decimal(6,2) NOT NULL,
  `joursLivraison` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `typesLivraison`
--

INSERT INTO `typesLivraison` (`id`, `libelle`, `coutTtc`, `joursLivraison`) VALUES
(1, 'Livraison Standard', '0.00', 10),
(2, 'Livraison Express', '5.00', 5);

-- --------------------------------------------------------

--
-- Structure de la table `typesMug`
--

CREATE TABLE `typesMug` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `typesMug`
--

INSERT INTO `typesMug` (`id`, `libelle`) VALUES
(1, 'Humour'),
(2, 'Mignon'),
(3, 'Romantique'),
(4, 'Vintage'),
(5, 'Action'),
(6, 'Marque'),
(7, 'Célébrité'),
(8, 'Animé');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `codePostal` int(5) DEFAULT NULL,
  `idCivilite` int(11) DEFAULT NULL,
  `idRole` int(11) NOT NULL DEFAULT '3',
  `avatar` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `newsletterAlert` tinyint(1) NOT NULL DEFAULT '0',
  `emailAlert` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `pseudo`, `nom`, `prenom`, `adresse`, `complement`, `ville`, `codePostal`, `idCivilite`, `idRole`, `avatar`, `createdAt`, `newsletterAlert`, `emailAlert`) VALUES
(1, 'admin@lebonmug.fr', '$2y$10$SPqkh5t5S2g2WJ.54l/QIe41BV7hyCPCCTpiOFV3GgFYSPxic.M.6', 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2020-02-09 10:38:35', 0, 1),
(2, 'sebastien@gmail.com', '$2y$10$Z68nw1aj8M0pbE7Iok7shOTYTMPPam8gg5EVng/kERyEsU2TSPZnO', 'Seb', 'Rutin', 'Sébastien', '17 rue des Lys', NULL, 'La Rochelle', 17000, 2, 3, 'avatar_user2.png', '2020-02-09 11:07:20', 0, 1),
(3, 'charles@gmail.com', '$2y$10$HYcOwtlJ3GFt8p2.A/WGDuXjXjPddjx8aJTgx5Bh4VJI6rRXoyKOS', 'Charles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2020-02-09 11:09:46', 1, 1),
(5, 'louis@gmail.com', '$2y$10$lxTQRsOVqIKHyKO2uXQSruRItl86HoWeFFeLMaIbq1S4iIrz3UG0O', 'Loulou', 'Louis', 'Daiquey', '45 avenue de La République', 'Appart. 21', 'Saintes', 17100, 2, 3, 'avatar_default_man.png', '2020-02-09 11:36:55', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `volumesMug`
--

CREATE TABLE `volumesMug` (
  `id` int(11) NOT NULL,
  `volume` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `volumesMug`
--

INSERT INTO `volumesMug` (`id`, `volume`) VALUES
(1, 250),
(2, 300),
(3, 350),
(4, 400);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `archiveMugs`
--
ALTER TABLE `archiveMugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCouleur` (`idCouleur`),
  ADD KEY `idEtatArticle` (`idEtatArticle`),
  ADD KEY `idStatutAnnonce` (`idStatutAnnonce`),
  ADD KEY `idType` (`idType`),
  ADD KEY `idVolume` (`idVolume`),
  ADD KEY `vendeur` (`idVendeur`);

--
-- Index pour la table `civilitesUser`
--
ALTER TABLE `civilitesUser`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idClient` (`idClient`),
  ADD KEY `idLivraison` (`idLivraison`);

--
-- Index pour la table `couleursMug`
--
ALTER TABLE `couleursMug`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etatsMug`
--
ALTER TABLE `etatsMug`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lignesCommande`
--
ALTER TABLE `lignesCommande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCommande` (`idCommande`),
  ADD KEY `idArticle` (`idArticle`);

--
-- Index pour la table `lignesPanierUser`
--
ALTER TABLE `lignesPanierUser`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArticle` (`idArticle`),
  ADD KEY `idUser` (`idUser`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArticle` (`idArticle`),
  ADD KEY `idStatutMessage` (`idStatutMessage`),
  ADD KEY `idSujet` (`idSujet`),
  ADD KEY `idAuteur` (`idAuteur`),
  ADD KEY `idDestinataire` (`idDestinataire`);

--
-- Index pour la table `mugs`
--
ALTER TABLE `mugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendeur` (`idVendeur`),
  ADD KEY `mugs_ibfk_1` (`idCouleur`),
  ADD KEY `mugs_ibfk_2` (`idEtat`),
  ADD KEY `mugs_ibfk_3` (`idStatut`),
  ADD KEY `mugs_ibfk_4` (`idType`),
  ADD KEY `mugs_ibfk_5` (`idVolume`);

--
-- Index pour la table `rolesUser`
--
ALTER TABLE `rolesUser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `libelle` (`libelle`);

--
-- Index pour la table `statutsMessage`
--
ALTER TABLE `statutsMessage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `statutsMug`
--
ALTER TABLE `statutsMug`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sujetsMessage`
--
ALTER TABLE `sujetsMessage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typesLivraison`
--
ALTER TABLE `typesLivraison`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typesMug`
--
ALTER TABLE `typesMug`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `idRole` (`idRole`),
  ADD KEY `idCivilite` (`idCivilite`);

--
-- Index pour la table `volumesMug`
--
ALTER TABLE `volumesMug`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `archiveMugs`
--
ALTER TABLE `archiveMugs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `civilitesUser`
--
ALTER TABLE `civilitesUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `couleursMug`
--
ALTER TABLE `couleursMug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `etatsMug`
--
ALTER TABLE `etatsMug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `lignesCommande`
--
ALTER TABLE `lignesCommande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `lignesPanierUser`
--
ALTER TABLE `lignesPanierUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `mugs`
--
ALTER TABLE `mugs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `rolesUser`
--
ALTER TABLE `rolesUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `statutsMessage`
--
ALTER TABLE `statutsMessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `statutsMug`
--
ALTER TABLE `statutsMug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sujetsMessage`
--
ALTER TABLE `sujetsMessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `typesLivraison`
--
ALTER TABLE `typesLivraison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `typesMug`
--
ALTER TABLE `typesMug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `volumesMug`
--
ALTER TABLE `volumesMug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `commandes_ibfk_2` FOREIGN KEY (`idLivraison`) REFERENCES `typesLivraison` (`id`);

--
-- Contraintes pour la table `lignesCommande`
--
ALTER TABLE `lignesCommande`
  ADD CONSTRAINT `lignescommande_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commandes` (`id`),
  ADD CONSTRAINT `lignescommande_ibfk_2` FOREIGN KEY (`idArticle`) REFERENCES `mugs` (`id`);

--
-- Contraintes pour la table `lignesPanierUser`
--
ALTER TABLE `lignesPanierUser`
  ADD CONSTRAINT `lignespanieruser_ibfk_1` FOREIGN KEY (`idArticle`) REFERENCES `mugs` (`id`),
  ADD CONSTRAINT `lignespanieruser_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `mugs`
--
ALTER TABLE `mugs`
  ADD CONSTRAINT `mugs_ibfk_1` FOREIGN KEY (`idCouleur`) REFERENCES `couleursMug` (`id`),
  ADD CONSTRAINT `mugs_ibfk_2` FOREIGN KEY (`idEtat`) REFERENCES `etatsMug` (`id`),
  ADD CONSTRAINT `mugs_ibfk_3` FOREIGN KEY (`idStatut`) REFERENCES `statutsMug` (`id`),
  ADD CONSTRAINT `mugs_ibfk_4` FOREIGN KEY (`idType`) REFERENCES `typesMug` (`id`),
  ADD CONSTRAINT `mugs_ibfk_5` FOREIGN KEY (`idVolume`) REFERENCES `volumesMug` (`id`),
  ADD CONSTRAINT `mugs_ibfk_6` FOREIGN KEY (`idVendeur`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
